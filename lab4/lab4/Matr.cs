﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class Matr
    {
        static Random rnd = new Random();

        static public int[][] CreateMatr(int n)
        {
            int[][] mtr = new int[n][];
            for (int i = 0; i < n; ++i)
            {
                int[] mass = new int[n];
                for (int j = 0; j < n; ++j)
                    mass[j] = rnd.Next(1, 100); 
                mtr[i] = mass;
            } 
                
            return mtr;
        }


        static public int[] SimpleDigits (int n, ref int[] mtr, out int size)
        {
            size = 0;
            int[] str = new int[n];
            for (int i = 0; i < n; ++i)
            {
                if (IsSimle(mtr[i]))
                {
                    str[size] = mtr[i];
                    ++size;
                }
            }
            return str;
        }


        static private bool IsSimle(int n)
        {
            if (n == 1) 
                return false;
            for (int d = 2; d * d <= n; d++)
            {
                if (n % d == 0)
                    return false;
            }
            return true;
        }


        static public void PrintMtr(string s, int[][] mtr)
        {
            Console.WriteLine(s+ ':');
            for (int i = 0; i <  mtr.Length; i++)
            {
                for (int j = 0; j < mtr[i].Length; j++)
                    Console.Write(  "{0,-3} ", mtr[i][j]);
                Console.WriteLine();
            }
            Console.WriteLine();

        }
    }
}
