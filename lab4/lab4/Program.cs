﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите размерность исходной матрицы NxN ");
            int n = Convert.ToInt32(Console.ReadLine());
            int[][] m = Matr.CreateMatr(n);
            Matr.PrintMtr("Исходная матрица",m);
            for (int i =0; i < n; i++)
            {
                int[] m2 = Matr.SimpleDigits(n, ref m[i], out int sz);
                int j = 0;
                if (sz == 0)
                {
                    Console.WriteLine("Строка {0}: не содержит простых чисел  ", i + 1);
                    continue;
                }
                Console.Write("Строка {0}: ", i + 1);
                while (j<sz && m2[j] != 0)
                {
                    Console.Write(" {0} ",m2[j]);
                    j++;
                }
                Console.WriteLine();
                
            }
            

            Console.ReadKey();
        }
    }
}
