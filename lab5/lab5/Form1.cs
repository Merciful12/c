using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace lab5
{
    public partial class Form1 : Form
    {
        string pathFile = "";
        

        public Form1()
        {
            InitializeComponent();
        }


        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pathFile = openFileDialog1.FileName;
                textBoxFile.Text = "";
                textBoxResult.Text = "";
                string readText = File.ReadAllText(pathFile);
                textBoxFile.Text = readText;
            }
        }

        
        private void создатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsCanDo("Создать"))
            {
                pathFile = "";
                textBoxFile.Text = "";
                textBoxFile.Text = "";
            }
        }


        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pathFile = saveFileDialog1.FileName;
                File.WriteAllText(pathFile, textBoxFile.Text);
            }
        }


        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pathFile == "")
                сохранитьКакToolStripMenuItem.PerformClick();
            else File.WriteAllText(pathFile, textBoxFile.Text);
        }


        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (IsCanDo("Выход"))
                Application.Exit();
        }

     
        private void выполнитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pathFile == "")
                открытьToolStripMenuItem.PerformClick();
            if (File.Exists(pathFile))
            {
                textBoxResult.Text = "";
                StreamReader sr = new StreamReader(pathFile);
                String str = "";
                while ((str = sr.ReadLine()) != null)
                {
                    if (str.Trim() != "")
                        textBoxResult.Text += str + (char)13 + (char)10;
                }
                sr.Close();
              
                
                очиститьToolStripMenuItem.Enabled = true;
                сохранитьрезToolStripMenuItem1.Enabled = true;
            }
        }


        private void очиститьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            textBoxResult.Text = "";
            очиститьToolStripMenuItem.Enabled = false;
            сохранитьрезToolStripMenuItem1.Enabled = false;
        }


        private void сохранитьрезToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string pathResult = saveFileDialog1.FileName;
                File.WriteAllText(pathResult, textBoxResult.Text);
            }
        }


        private void оЗаданииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Дан текстовый файл, слова в котором состоят только из русских букв. " +
                            "Переписать его содержимое в новый файл, " +
                            "сохраняя строчную структуру и удаляя пустые строки. "+
                            "Строка, состоящая только из одних пробелов, тоже считается пустой.",
                            "Task",
                            MessageBoxButtons.OK);
        }


        private void Form1_FormClosing_1(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !IsCanDo("Выход");
        }


        private bool IsCanDo(string msg)
        {
            bool Can = true;
            if (textBoxFile.Text != "")
            {
                switch (MessageBox.Show("Сохранить изменения?", msg, MessageBoxButtons.YesNoCancel))
                {
                    case DialogResult.Yes:
                        if (pathFile == "")
                            сохранитьКакToolStripMenuItem.PerformClick();
                        else сохранитьToolStripMenuItem.PerformClick();
                        break;
                    case DialogResult.No:
                        break;
                    case DialogResult.Cancel:
                        Can = false;
                        break;
                }
            }
            return Can;
        }

       
    }
}
