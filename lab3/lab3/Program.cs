﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите кол-во эл-ов массива");
            int n = Int32.Parse(Console.ReadLine());
            int[] arr = new int[n];
            Console.WriteLine("Введите массив через пробел");
            string[] s = Console.ReadLine().Split(' ');
            for (int j = 0; j < n; j++)
            {
                arr[j] = Convert.ToInt32(s[j]);
                Console.Write("{0} ",arr[j]);
            }
            Console.WriteLine();
            int res = 0;
            int i = 0;
            while (i < n-1)
            {
                if (arr[i] > arr[i + 1])
                {
                    res++;
                    while (i < n - 1 && arr[i] > arr[i + 1]) i++;
                }
                else if (arr[i] < arr[i + 1])
                {
                    res++;
                    while (i < n - 1 && arr[i] < arr[i + 1]) i++;
                }
                i++;
            }

            Console.WriteLine(res);
            Console.ReadKey();
        }
    }
}
