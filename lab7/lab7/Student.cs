﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab7
{
    
    [Serializable]
    public class Student
    {
        [Serializable]
        public struct Session
        {
            public string[] subject;
            public int[] mark;
        }

        public string FIO;
        public int cours;
        public int group;
        public string form;
        public int cntSession;
        public Session[] sessions;


        public Student() { }
        public Student(string fio, int cours, int group, string form, int cntSession)
        {
            this.FIO = fio;
            this.cours = cours;
            this.group = group;
            this.form = form;
            this.cntSession = cntSession;
            this.sessions = new Session[cntSession];
            for (int i = 0; i < cntSession; i++)
            {

                this.sessions[i].mark = new int[5];
                this.sessions[i].subject = new string[5];
            }
        }
        

        public  float Average()
        {
            int sum = 0;
            float res = 0;
            if (this.cntSession > 0)
            {
                int lastSes = this.cntSession - 1;
                for (int j = 0; j < 5; j++)
                    sum += this.sessions[lastSes].mark[j];

                res = (float)sum / 5;
            }

            return res;
        }

    }
}
