﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab7
{
   
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            Init(ref btns);
        }

        Students students;
        Button[] btns;

        SideForm sdForm = new SideForm();
        TaskRes taskRes = new TaskRes();
       
        string path,
               extension;


        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
           // openFileDialog1.CheckFileExists = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                path = openFileDialog1.FileName;
                extension = openFileDialog1.SafeFileName.Split('.').Last().ToLower();
                WorkWithFiles.OpenFile(ref students,path, extension);
                FieldsVisible(false, btns);
                BtnNew.Enabled = true;
                ВыполнитьToolStripMenuItem.Enabled = true;
                if (students.Count == 0)                
                    MessageBox.Show("Файл пуст");                
                
                else
                    SetList(ListOFStudents);      
            }
        }


        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WorkWithFiles.SaveFile(students, path, extension);
        }


        private void сохранитьКакToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string pathSave = saveFileDialog1.FileName;
                string extensionSave = saveFileDialog1.FileName.Split('.').Last().ToLower();
                WorkWithFiles.SaveFile(students, pathSave, extensionSave);
            }
        }


        private void создатьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                path = saveFileDialog1.FileName;
                extension = saveFileDialog1.FileName.Split('.').Last().ToLower();
                ListOFStudents.Items.Clear();
                students = new Students();
                WorkWithFiles.SaveFile(students, path, extension);
                BtnNew.Enabled = true;
                ВыполнитьToolStripMenuItem.Enabled = true;
            }
        }


        private void ВыполнитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            taskRes.TxtBoxRes.Clear();
            for (int i = 1; i < 5; i++)
            {
                taskRes.TxtBoxRes.Text += String.Format("\tКурс {0}\t", i) + Environment.NewLine;
                var studentsInCours = students.myTask(i);
                if (studentsInCours.Count == 0)
                    taskRes.TxtBoxRes.Text += "Нет студентов" + Environment.NewLine;
                else
                {
                    foreach (var group in studentsInCours)
                        taskRes.TxtBoxRes.Text += String.Format("Группа: {0} Средний балл: {1}",
                                            group.Key, group.Value) + Environment.NewLine;
                }
                taskRes.TxtBoxRes.Text += Environment.NewLine;
            }            
            taskRes.ShowDialog();
        }


        private void оЗаданииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("На каждом курсе вывести средние баллы каждой группы в порядке убывания");
        }


        private void BtnNew_Click(object sender, EventArgs e)
        {
            if (sdForm.AddSt())
            {
                students.Add(sdForm.st);
                SetList(ListOFStudents);
            }
            FieldsVisible(false, btns);

        }


        private void BtnEdit_Click(object sender, EventArgs e)
        {
            int numOfSt = ListOFStudents.SelectedIndex;
            sdForm.st = students[numOfSt];
            if (sdForm.ChengeSt())
            {
                students[numOfSt] = sdForm.st;
                SetList(ListOFStudents);
            }
            FieldsVisible(false, btns);
        }


        private void BtnShow_Click(object sender, EventArgs e)
        {
            int numOfSt = ListOFStudents.SelectedIndex;
            sdForm.ShowSt(students[numOfSt]);
        }


        private void BtnDel_Click(object sender, EventArgs e)
        {
            students.Del(ListOFStudents.SelectedIndex);
            SetList(ListOFStudents);
            FieldsVisible(false, btns);
        }


        private void Init(ref Button[] btn)
        {
            btn = new Button[] {BtnShow, BtnEdit, BtnDel };
        }


        private void SetList(ListBox list)
        {
            list.Items.Clear();
            for (int i = 0; i < students.Count;++i)
            {
                string s = String.Format("{0} {1} {2}", students[i].FIO, students[i].cours, students[i].group);
                list.Items.Add(s);
            }
        }


        private void ListOFStudents_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListOFStudents.SelectedIndex != -1)
                FieldsVisible(true, btns);
        }


        private void FieldsVisible(bool enable, Button[] btn)
        {
            for (int i = 0, len = btn.Length; i < len; i++)
            {
                btn[i].Enabled = enable;
            }
        }


    }
}
