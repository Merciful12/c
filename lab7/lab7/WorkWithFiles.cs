﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab7
{   
    
    


    class WorkWithFiles
    {
        static public void OpenFile(ref Students students, string path, string extension)
        {

            students = new Students();
            if (extension == "txt")  OpenTxt(students,path);
            else if (extension == "xml")  OpenXml(ref students, path);
            else  OpenBinary(ref students, path);
            
        }

        static public void SaveFile(Students students, string path, string extension)
        {
            if (extension == "txt")  SaveTxt(students, path);
            else if (extension == "xml") SaveXml(students, path); 
            else  SaveBinary(students, path);
        }

        private static  void OpenBinary(ref Students students, string path)
        {
            
            FileStream fs = File.OpenRead(path);
            BinaryFormatter bf = new BinaryFormatter();

            if (fs.Length > 0) students = (Students)bf.Deserialize(fs);
            
            fs.Close();
            
           
        }

        private static  void SaveBinary(Students students, string path)
        {
            FileStream sw = File.OpenWrite(path);
            BinaryFormatter bf = new BinaryFormatter();
            
            bf.Serialize(sw,students);
            
            sw.Close();
        }


        private static void OpenXml(ref Students students, string path)
        {
           
            FileStream fs = new FileStream(path, FileMode.Open);
            XmlSerializer xml = new XmlSerializer(typeof(Students));

            if (fs.Length > 0) students = (Students)xml.Deserialize(fs);

            fs.Close();
            
        }


        private static void SaveXml(Students students, string path)
        {
            XmlSerializer xml = new XmlSerializer(typeof(Students));
            TextWriter sw = new StreamWriter(path);

            xml.Serialize(sw, students);

            sw.Close();
        }

        private static void OpenTxt(Students students, string path)
        {
            
            StreamReader sr = new StreamReader(path);
            String str = "";
            while ((str = sr.ReadLine()) != null)
            {
                string[] s = str.Split(',');
                string FIO = s[0];
                int cours = Convert.ToInt32(s[1]);
                int group = Convert.ToInt32(s[2]);
                string form = s[3].TrimStart();
                int cntSession  = Convert.ToInt32(s[4]);
                Student st = new Student(FIO,cours,group,form,cntSession);
                for (int j = 0; j < cntSession; j++)
                {
                    
                    sr.ReadLine(); // пропуск строки с номером сессии
                  
                    for (int k = 0; k < 5; k++)
                    {
                        str = sr.ReadLine();
                        s = str.Split(',');                        
                        st.sessions[j].subject[k] = s[0];
                        st.sessions[j].mark[k] = Convert.ToInt32(s[1]);
                    }
                }
                students.arrayOfStudent.Add(st);
            }
            sr.Close();
        }

        private static void SaveTxt(Students students, string path)
        {
            StreamWriter sw = new StreamWriter(path);
            foreach (Student st in students.arrayOfStudent)
            {
                string s = String.Format("{0}, {1}, {2}, {3}, {4}",st.FIO, st.cours,st.group, st.form, st.cntSession);
                sw.WriteLine(s);
                for (int i = 0; i < st.cntSession; i++)
                {
                    s = String.Format("СЕССИЯ {0}", i + 1);
                    sw.WriteLine(s);
                    for (int j = 0; j < 5; j++)
                    {
                        s = String.Format("{0}, {1}", st.sessions[i].subject[j], st.sessions[i].mark[j]);
                        sw.WriteLine(s);
                    }
                    
                }
                
            }
            sw.Close();


        }
    }
}
