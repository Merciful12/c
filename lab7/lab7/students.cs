﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab7
{
    [Serializable]
    public class Students
    {

        public List<Student> arrayOfStudent; //Student[] arrayOfStudent;
        public int Count { get { return arrayOfStudent.Count; } }


        public Students()
        {
            arrayOfStudent = new List<Student>();
        }


        public Student this[int index]
        {
            get
            {
                return arrayOfStudent[index];
            }
            set
            {
                arrayOfStudent[index] = value;
            }
        }


        public void Del(int numberOfStudent)
        {
            arrayOfStudent.RemoveAt(numberOfStudent);
        }


        public void Add(Student st)
        {
            arrayOfStudent.Add(st);
        }


        public List<KeyValuePair<int, float>> myTask(int curs)
        {
            Dictionary<int, float> group_sum = new Dictionary<int, float>();

            Dictionary<int, int> group_cnt = new Dictionary<int, int>();

            float averageOfSt;
            foreach (Student st in this.arrayOfStudent)
            {
                if (curs == st.cours)
                {
                    averageOfSt = st.Average();
                    if (group_sum.ContainsKey(st.group))
                    {
                        group_sum[st.group] += averageOfSt;
                        ++group_cnt[st.group];

                    }
                    else
                    {
                        group_sum.Add(st.group, averageOfSt);
                        group_cnt.Add(st.group, 1);
                    }
                }
            }
            ICollection<int> keys = group_cnt.Keys;
            foreach (int key in keys)
            {
                group_sum[key] /= group_cnt[key];
            }


            var sortedElements = group_sum.OrderByDescending(kvp => kvp.Value);
            return sortedElements.ToList();
        }


    }
}
