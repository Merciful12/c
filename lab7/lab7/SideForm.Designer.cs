﻿namespace lab7
{
    partial class SideForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.BtnClose = new System.Windows.Forms.Button();
            this.LblFIO = new System.Windows.Forms.Label();
            this.LblCours = new System.Windows.Forms.Label();
            this.LblGroup = new System.Windows.Forms.Label();
            this.LblForm = new System.Windows.Forms.Label();
            this.LblProgress = new System.Windows.Forms.Label();
            this.CmbBoxCours = new System.Windows.Forms.ComboBox();
            this.CmbBoxGroup = new System.Windows.Forms.ComboBox();
            this.CmbBoxForm = new System.Windows.Forms.ComboBox();
            this.TxtBoxFIO = new System.Windows.Forms.TextBox();
            this.TxtBoxCours = new System.Windows.Forms.TextBox();
            this.TxtBoxGroup = new System.Windows.Forms.TextBox();
            this.TxtBoxForm = new System.Windows.Forms.TextBox();
            this.DGVSessions = new System.Windows.Forms.DataGridView();
            this.TxtBoxSes = new System.Windows.Forms.TextBox();
            this.LblCntSession = new System.Windows.Forms.Label();
            this.CmbBoxcntSession = new System.Windows.Forms.ComboBox();
            this.BtnSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DGVSessions)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnClose
            // 
            this.BtnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BtnClose.Location = new System.Drawing.Point(468, 618);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Size = new System.Drawing.Size(75, 23);
            this.BtnClose.TabIndex = 3;
            this.BtnClose.Text = "Закрыть";
            this.BtnClose.UseVisualStyleBackColor = true;
            this.BtnClose.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // LblFIO
            // 
            this.LblFIO.AutoSize = true;
            this.LblFIO.Location = new System.Drawing.Point(91, 69);
            this.LblFIO.Name = "LblFIO";
            this.LblFIO.Size = new System.Drawing.Size(34, 13);
            this.LblFIO.TabIndex = 5;
            this.LblFIO.Text = "ФИО";
            // 
            // LblCours
            // 
            this.LblCours.AutoSize = true;
            this.LblCours.Location = new System.Drawing.Point(91, 121);
            this.LblCours.Name = "LblCours";
            this.LblCours.Size = new System.Drawing.Size(31, 13);
            this.LblCours.TabIndex = 6;
            this.LblCours.Text = "Курс";
            // 
            // LblGroup
            // 
            this.LblGroup.AutoSize = true;
            this.LblGroup.Location = new System.Drawing.Point(91, 175);
            this.LblGroup.Name = "LblGroup";
            this.LblGroup.Size = new System.Drawing.Size(42, 13);
            this.LblGroup.TabIndex = 7;
            this.LblGroup.Text = "Группа";
            // 
            // LblForm
            // 
            this.LblForm.AutoSize = true;
            this.LblForm.Location = new System.Drawing.Point(91, 229);
            this.LblForm.Name = "LblForm";
            this.LblForm.Size = new System.Drawing.Size(93, 13);
            this.LblForm.TabIndex = 8;
            this.LblForm.Text = "Форма обучения";
            // 
            // LblProgress
            // 
            this.LblProgress.AutoSize = true;
            this.LblProgress.Location = new System.Drawing.Point(225, 335);
            this.LblProgress.Name = "LblProgress";
            this.LblProgress.Size = new System.Drawing.Size(82, 13);
            this.LblProgress.TabIndex = 9;
            this.LblProgress.Text = "Успеваемость";
            // 
            // CmbBoxCours
            // 
            this.CmbBoxCours.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CmbBoxCours.FormattingEnabled = true;
            this.CmbBoxCours.Location = new System.Drawing.Point(219, 116);
            this.CmbBoxCours.Name = "CmbBoxCours";
            this.CmbBoxCours.Size = new System.Drawing.Size(36, 23);
            this.CmbBoxCours.TabIndex = 14;
            this.CmbBoxCours.Visible = false;
            // 
            // CmbBoxGroup
            // 
            this.CmbBoxGroup.FormattingEnabled = true;
            this.CmbBoxGroup.Location = new System.Drawing.Point(219, 172);
            this.CmbBoxGroup.Name = "CmbBoxGroup";
            this.CmbBoxGroup.Size = new System.Drawing.Size(64, 21);
            this.CmbBoxGroup.TabIndex = 15;
            this.CmbBoxGroup.Visible = false;
            // 
            // CmbBoxForm
            // 
            this.CmbBoxForm.FormattingEnabled = true;
            this.CmbBoxForm.Location = new System.Drawing.Point(219, 226);
            this.CmbBoxForm.Name = "CmbBoxForm";
            this.CmbBoxForm.Size = new System.Drawing.Size(129, 21);
            this.CmbBoxForm.TabIndex = 16;
            this.CmbBoxForm.Visible = false;
            // 
            // TxtBoxFIO
            // 
            this.TxtBoxFIO.Location = new System.Drawing.Point(219, 69);
            this.TxtBoxFIO.Name = "TxtBoxFIO";
            this.TxtBoxFIO.ReadOnly = true;
            this.TxtBoxFIO.Size = new System.Drawing.Size(185, 20);
            this.TxtBoxFIO.TabIndex = 17;
            // 
            // TxtBoxCours
            // 
            this.TxtBoxCours.Location = new System.Drawing.Point(219, 118);
            this.TxtBoxCours.Name = "TxtBoxCours";
            this.TxtBoxCours.ReadOnly = true;
            this.TxtBoxCours.Size = new System.Drawing.Size(36, 20);
            this.TxtBoxCours.TabIndex = 18;
            // 
            // TxtBoxGroup
            // 
            this.TxtBoxGroup.Location = new System.Drawing.Point(219, 172);
            this.TxtBoxGroup.Name = "TxtBoxGroup";
            this.TxtBoxGroup.ReadOnly = true;
            this.TxtBoxGroup.Size = new System.Drawing.Size(64, 20);
            this.TxtBoxGroup.TabIndex = 19;
            // 
            // TxtBoxForm
            // 
            this.TxtBoxForm.Location = new System.Drawing.Point(219, 226);
            this.TxtBoxForm.Name = "TxtBoxForm";
            this.TxtBoxForm.ReadOnly = true;
            this.TxtBoxForm.Size = new System.Drawing.Size(129, 20);
            this.TxtBoxForm.TabIndex = 20;
            // 
            // DGVSessions
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DGVSessions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.DGVSessions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DGVSessions.DefaultCellStyle = dataGridViewCellStyle8;
            this.DGVSessions.Location = new System.Drawing.Point(135, 362);
            this.DGVSessions.Name = "DGVSessions";
            this.DGVSessions.ReadOnly = true;
            this.DGVSessions.Size = new System.Drawing.Size(260, 234);
            this.DGVSessions.TabIndex = 21;
            // 
            // TxtBoxSes
            // 
            this.TxtBoxSes.Location = new System.Drawing.Point(219, 276);
            this.TxtBoxSes.Name = "TxtBoxSes";
            this.TxtBoxSes.ReadOnly = true;
            this.TxtBoxSes.Size = new System.Drawing.Size(36, 20);
            this.TxtBoxSes.TabIndex = 24;
            // 
            // LblCntSession
            // 
            this.LblCntSession.AutoSize = true;
            this.LblCntSession.Location = new System.Drawing.Point(91, 279);
            this.LblCntSession.Name = "LblCntSession";
            this.LblCntSession.Size = new System.Drawing.Size(105, 13);
            this.LblCntSession.TabIndex = 25;
            this.LblCntSession.Text = "Количество сессий";
            // 
            // CmbBoxcntSession
            // 
            this.CmbBoxcntSession.FormattingEnabled = true;
            this.CmbBoxcntSession.Location = new System.Drawing.Point(219, 276);
            this.CmbBoxcntSession.Name = "CmbBoxcntSession";
            this.CmbBoxcntSession.Size = new System.Drawing.Size(36, 21);
            this.CmbBoxcntSession.TabIndex = 26;
            this.CmbBoxcntSession.Visible = false;
            // 
            // BtnSave
            // 
            this.BtnSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.BtnSave.Location = new System.Drawing.Point(370, 618);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 27;
            this.BtnSave.Text = "Сохранить";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // SideForm
            // 
            this.AcceptButton = this.BtnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BtnClose;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(572, 670);
            this.Controls.Add(this.BtnSave);
            this.Controls.Add(this.LblCntSession);
            this.Controls.Add(this.TxtBoxSes);
            this.Controls.Add(this.DGVSessions);
            this.Controls.Add(this.TxtBoxForm);
            this.Controls.Add(this.TxtBoxGroup);
            this.Controls.Add(this.TxtBoxCours);
            this.Controls.Add(this.TxtBoxFIO);
            this.Controls.Add(this.CmbBoxForm);
            this.Controls.Add(this.CmbBoxGroup);
            this.Controls.Add(this.CmbBoxCours);
            this.Controls.Add(this.LblProgress);
            this.Controls.Add(this.LblForm);
            this.Controls.Add(this.LblGroup);
            this.Controls.Add(this.LblCours);
            this.Controls.Add(this.LblFIO);
            this.Controls.Add(this.BtnClose);
            this.Controls.Add(this.CmbBoxcntSession);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SideForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Student";
            ((System.ComponentModel.ISupportInitialize)(this.DGVSessions)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BtnClose;
        private System.Windows.Forms.Label LblFIO;
        private System.Windows.Forms.Label LblCours;
        private System.Windows.Forms.Label LblGroup;
        private System.Windows.Forms.Label LblForm;
        private System.Windows.Forms.Label LblProgress;
        private System.Windows.Forms.ComboBox CmbBoxCours;
        private System.Windows.Forms.ComboBox CmbBoxGroup;
        private System.Windows.Forms.ComboBox CmbBoxForm;
        private System.Windows.Forms.TextBox TxtBoxFIO;
        private System.Windows.Forms.TextBox TxtBoxCours;
        private System.Windows.Forms.TextBox TxtBoxGroup;
        private System.Windows.Forms.TextBox TxtBoxForm;
        private System.Windows.Forms.DataGridView DGVSessions;
        private System.Windows.Forms.TextBox TxtBoxSes;
        private System.Windows.Forms.Label LblCntSession;
        private System.Windows.Forms.ComboBox CmbBoxcntSession;
        private System.Windows.Forms.Button BtnSave;
    }
}

