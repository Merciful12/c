﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab7
{
    
    public partial class SideForm : Form
    {
        public SideForm()
        {
            InitializeComponent();
            Init(ref txtFields, ref cmbFields, ref btns);
            SetCombo(cmbFields);
        }

        TextBox[] txtFields;
        ComboBox[] cmbFields;
        Button[] btns;
        public Student st;


        public void ShowSt(Student st)
        {
            DoFieldsEmpty(DGVSessions);
            AllFieldsVisible(true);
            WorkWithStudents.Show(st, txtFields, cmbFields, DGVSessions);
            BtnSave.Visible = false;
            ShowDialog();
        }


        public bool AddSt()
        {
            DoFieldsEmpty(txtFields);
            DoFieldsEmpty(cmbFields);
            DoFieldsEmpty(DGVSessions);
            AllFieldsVisible(false);
            BtnSave.Visible = true;

            return ShowDialog() == DialogResult.OK;
        }


        public bool ChengeSt()
        {
            DoFieldsEmpty(DGVSessions);
            WorkWithStudents.Show(st, txtFields, cmbFields, DGVSessions);
            AllFieldsVisible(false);
            BtnSave.Visible = true;

            return ShowDialog() == DialogResult.OK;
           
        }


        private void BtnOK_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void BtnSave_Click(object sender, EventArgs e)
        {
            WorkWithStudents.Change(ref this.st, cmbFields, TxtBoxFIO, DGVSessions);
            Close();
        }


        


        void AllFieldsVisible(bool visible)
        {
            FieldsVisible(visible, txtFields);
            FieldsVisible(!visible, cmbFields);
            DGVSessions.ReadOnly = visible;
        }



        void FieldsVisible(bool enable, TextBox[] fields)
        {

            fields[0].ReadOnly = enable;
            for (int i = 1, len = fields.Length; i < len; i++)
            {
                fields[i].Visible = enable;
            }
        }


        void FieldsVisible(bool enable, ComboBox[] cmb)
        {
            for (int i = 0, len = cmb.Length; i < len; i++)
            {
                cmb[i].Visible = enable;
            }
        }


        void FieldsVisible(bool enable, Button[] btn)
        {
            for (int i = 0, len = btn.Length; i < len; i++)
            {
                btn[i].Enabled = enable;
            }
            
        }


        private void DoFieldsEmpty(DataGridView dgv)
        {

            DataTable matr = new DataTable("matr");
            DataColumn[] cols = new DataColumn[2];
            cols[0] = new DataColumn("Предмет");
            matr.Columns.Add(cols[0]);
            cols[1] = new DataColumn("Оценка");
            matr.Columns.Add(cols[1]);
            for (int i = 0; i < 8 * 6; i++)
            {
                DataRow newRow;
                newRow = matr.NewRow();
                matr.Rows.Add(newRow);
            }
            dgv.DataSource = matr;

            DataGridViewCell txtCell;

            for (int i = 0; i < 8 * 6;)
            {
                for (int j = 0; j < 8; j++)
                {
                    txtCell = dgv.Rows[i].Cells[0];
                    txtCell.Value = "СЕССИЯ";
                    txtCell = dgv.Rows[i].Cells[1];
                    txtCell.Value = (j + 1).ToString();
                    i += 6;
                }
            }
        }

        private void Init(ref TextBox[] fields, ref ComboBox[] cmb, ref Button[] btn)
        {
            fields = new TextBox[] { TxtBoxFIO, TxtBoxCours, TxtBoxGroup, TxtBoxForm, TxtBoxSes };
            cmb = new ComboBox[] { CmbBoxCours, CmbBoxGroup, CmbBoxForm, CmbBoxcntSession };
            btn = new Button[] {BtnClose};

        }

        
        private void SetCombo(ComboBox[] cmb)
        {
            //cmb  { CmbBoxCours, CmbBoxGroup, CmbBoxForm, CmbBoxcntSession };
            System.Object[] curs = { 1, 2, 3, 4 };
            cmb[0].Items.AddRange(curs);

            for (int i = 1; i < 101; i++)
            {
                cmb[1].Items.Add(i);
            }

            System.Object[] forms = { "очная", "заочная", "вечерняя" };
            cmb[2].Items.AddRange(forms);

            System.Object[] cntSes = { 1, 2, 3, 4, 5, 6, 7, 8 };
            cmb[3].Items.AddRange(cntSes);
        }


        private void DoFieldsEmpty(TextBox[] fields)
        {
            fields[0].Text = "ФИО";
            for (int i = 1, len = fields.Length; i < len; i++)
            {
                fields[i].Text = "1";
            }
        }


        private void DoFieldsEmpty(ComboBox[] cmb)
        {
            for (int i = 0, len = cmb.Length; i < len; i++)
            {
                cmb[i].Text = "1";
            }
        }

    }
}
