﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab7
{
    
    
    class WorkWithStudents
    {
        static public void Show(Student st, TextBox[] fields, ComboBox[] cmb, DataGridView dgv)
        {

            fields[0].Text = st.FIO;
            cmb[0].Text = fields[1].Text = Convert.ToString(st.cours);
            cmb[1].Text = fields[2].Text = Convert.ToString(st.group);
            cmb[2].Text = fields[3].Text = st.form;
            cmb[3].Text = fields[4].Text = Convert.ToString(st.cntSession);
            DataGridViewCell txtCell;
           
            for (int i = 0; i < st.cntSession * 6;)
            {
                for (int j = 0; j < st.cntSession; j++)
                {
                    for (int k = 0; k < 5; k++)
                    {
                        txtCell = dgv.Rows[i+k+1].Cells[0];
                        txtCell.Value = st.sessions[j].subject[k];

                        txtCell = dgv.Rows[i+k+1].Cells[1];
                        txtCell.Value = st.sessions[j].mark[k].ToString();
                    }
                    i += 6;
                }
            }
            
        
        }
        

        static public void Change(ref Student st, ComboBox[] cmb, TextBox fio, DataGridView dgv)
        {
            string FIO = fio.Text;
            int cours = (cmb[0].Text == "") ? 0 : Int32.Parse(cmb[0].Text);
            int group = (cmb[1].Text == "") ? 0 : Int32.Parse(cmb[1].Text);
            string form = cmb[2].Text;
            int cntSession = (cmb[3].Text == "") ? 0 : Int32.Parse(cmb[3].Text);
            st = new Student(FIO,cours,group,form,cntSession);
            DataGridViewCell txtCell;
            string s;
            for (int i = 0; i < cntSession * 6;)
            {
                for (int j = 0; j < cntSession; j++)
                {
                    ++i; // перепрыгиваем строку с номером сессии
                    for (int k = 0; k < 5; k++)
                    {
                        txtCell = dgv.Rows[i].Cells[0];
                        s = txtCell.Value.ToString();
                        st.sessions[j].subject[k] = (s == "") ? "Предмет " : s;

                        txtCell = dgv.Rows[i].Cells[1];
                        s = txtCell.Value.ToString();
                        st.sessions[j].mark[k] = (s == "") ? 0 : Int32.Parse(s); 
                        ++i;
                    }
                    
                }
            }
        } 


        
    }
}