﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab6
{
    public partial class Form1 : Form
    {
        int N;
        public Form1()
        {
            InitializeComponent();
        }


        private void BtnOK_Click(object sender, EventArgs e)
        {
            try
            {
                N = Int32.Parse(TxtInputSize.Text);
                if ( !(N > 0) )
                {
                    throw new FormatException();
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("В этом поле должно быть натуральное число( > 0 )");
                return;
            }

            DataTable matr = new DataTable("matr");
            DataColumn[] cols = new DataColumn[N];

            for (int i = 0; i < N; i++)
            {
                cols[i] = new DataColumn((i+1).ToString());
                matr.Columns.Add(cols[i]);
            }

            for (int i = 0; i < N; i++)
            {
                DataRow newRow;
                newRow = matr.NewRow();
                matr.Rows.Add(newRow);
            }

            datGrdViewIn.DataSource = matr;

            for (int i = 0; i < N; i++)
            {
                datGrdViewIn.Columns[i].Width = 50;
            }

            labelSrc.Visible = true;
            labelRes.Visible = false;
            datGrdViewIn.Visible = true;
            BtnPerform.Enabled = true;
            datGrdViewRes.Visible = false;

        }

        private void BtnPerform_Click(object sender, EventArgs e)
        {
            Matr mtr = new Matr(N);
            mtr.GridToMatr(datGrdViewIn);
            mtr.Task();
            mtr.MatrToGrid(datGrdViewRes);

            labelRes.Visible = true;
            datGrdViewIn.Dock = DockStyle.Left;
            datGrdViewRes.Visible = true;
        }


        private void оЗаданииToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("После строк, элементы которых строго возрастают, " +
                "вставить строки, содержащие те же элементы в обратном порядке."); ;
        }
    }
}
