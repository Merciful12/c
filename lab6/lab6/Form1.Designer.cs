﻿using System;

namespace lab6
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtInputSize = new System.Windows.Forms.TextBox();
            this.labelSize = new System.Windows.Forms.Label();
            this.BtnOK = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.оЗаданииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.datGrdViewIn = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.datGrdViewRes = new System.Windows.Forms.DataGridView();
            this.labelSrc = new System.Windows.Forms.Label();
            this.labelRes = new System.Windows.Forms.Label();
            this.BtnPerform = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datGrdViewIn)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datGrdViewRes)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtInputSize
            // 
            this.TxtInputSize.Location = new System.Drawing.Point(12, 67);
            this.TxtInputSize.Name = "TxtInputSize";
            this.TxtInputSize.Size = new System.Drawing.Size(115, 20);
            this.TxtInputSize.TabIndex = 0;
            // 
            // labelSize
            // 
            this.labelSize.AutoSize = true;
            this.labelSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSize.Location = new System.Drawing.Point(9, 48);
            this.labelSize.Name = "labelSize";
            this.labelSize.Size = new System.Drawing.Size(118, 16);
            this.labelSize.TabIndex = 1;
            this.labelSize.Text = "Размер матрицы";
            // 
            // BtnOK
            // 
            this.BtnOK.Location = new System.Drawing.Point(156, 67);
            this.BtnOK.Name = "BtnOK";
            this.BtnOK.Size = new System.Drawing.Size(75, 20);
            this.BtnOK.TabIndex = 2;
            this.BtnOK.Text = "OK";
            this.BtnOK.UseVisualStyleBackColor = true;
            this.BtnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.оЗаданииToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(704, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // оЗаданииToolStripMenuItem
            // 
            this.оЗаданииToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.оЗаданииToolStripMenuItem.Name = "оЗаданииToolStripMenuItem";
            this.оЗаданииToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F7;
            this.оЗаданииToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.оЗаданииToolStripMenuItem.Text = "О задании";
            this.оЗаданииToolStripMenuItem.Click += new System.EventHandler(this.оЗаданииToolStripMenuItem_Click);
            // 
            // datGrdViewIn
            // 
            this.datGrdViewIn.AllowUserToAddRows = false;
            this.datGrdViewIn.AllowUserToDeleteRows = false;
            this.datGrdViewIn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datGrdViewIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datGrdViewIn.Location = new System.Drawing.Point(0, 0);
            this.datGrdViewIn.Name = "datGrdViewIn";
            this.datGrdViewIn.Size = new System.Drawing.Size(704, 424);
            this.datGrdViewIn.TabIndex = 4;
            this.datGrdViewIn.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.datGrdViewRes);
            this.panel1.Controls.Add(this.datGrdViewIn);
            this.panel1.Location = new System.Drawing.Point(0, 131);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(704, 424);
            this.panel1.TabIndex = 5;
            // 
            // datGrdViewRes
            // 
            this.datGrdViewRes.AllowUserToAddRows = false;
            this.datGrdViewRes.AllowUserToDeleteRows = false;
            this.datGrdViewRes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datGrdViewRes.Dock = System.Windows.Forms.DockStyle.Right;
            this.datGrdViewRes.Location = new System.Drawing.Point(315, 0);
            this.datGrdViewRes.Name = "datGrdViewRes";
            this.datGrdViewRes.ReadOnly = true;
            this.datGrdViewRes.Size = new System.Drawing.Size(389, 424);
            this.datGrdViewRes.TabIndex = 5;
            this.datGrdViewRes.Visible = false;
            // 
            // labelSrc
            // 
            this.labelSrc.AutoSize = true;
            this.labelSrc.Location = new System.Drawing.Point(102, 115);
            this.labelSrc.Name = "labelSrc";
            this.labelSrc.Size = new System.Drawing.Size(102, 13);
            this.labelSrc.TabIndex = 6;
            this.labelSrc.Text = "Исходная матрица";
            this.labelSrc.Visible = false;
            // 
            // labelRes
            // 
            this.labelRes.AutoSize = true;
            this.labelRes.Location = new System.Drawing.Point(491, 115);
            this.labelRes.Name = "labelRes";
            this.labelRes.Size = new System.Drawing.Size(59, 13);
            this.labelRes.TabIndex = 7;
            this.labelRes.Text = "Результат";
            this.labelRes.Visible = false;
            // 
            // BtnPerform
            // 
            this.BtnPerform.Enabled = false;
            this.BtnPerform.Location = new System.Drawing.Point(390, 48);
            this.BtnPerform.Name = "BtnPerform";
            this.BtnPerform.Size = new System.Drawing.Size(172, 44);
            this.BtnPerform.TabIndex = 8;
            this.BtnPerform.Text = "Выполнить задание";
            this.BtnPerform.UseVisualStyleBackColor = true;
            this.BtnPerform.Click += new System.EventHandler(this.BtnPerform_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 567);
            this.Controls.Add(this.BtnPerform);
            this.Controls.Add(this.labelRes);
            this.Controls.Add(this.labelSrc);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.BtnOK);
            this.Controls.Add(this.labelSize);
            this.Controls.Add(this.TxtInputSize);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Lab6";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datGrdViewIn)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.datGrdViewRes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        

        #endregion

        private System.Windows.Forms.TextBox TxtInputSize;
        private System.Windows.Forms.Label labelSize;
        private System.Windows.Forms.Button BtnOK;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem оЗаданииToolStripMenuItem;
        private System.Windows.Forms.DataGridView datGrdViewIn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelSrc;
        private System.Windows.Forms.Label labelRes;
        private System.Windows.Forms.Button BtnPerform;
        private System.Windows.Forms.DataGridView datGrdViewRes;
    }
}

