﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

namespace lab6
{
    class Matr
    {
        int nStr;
        int nCol;
        int[][] matrix;


        public Matr(int n)
        {
            nStr = n;
            nCol = n;
            matrix = new int[n][];
            for (int i = 0; i < nStr; i++)
            {
                matrix[i] = new int[nCol];
            }
        }


        public void GridToMatr(DataGridView dgv)
        {
            DataGridViewCell txtCell;
            for (int i = 0; i < nStr; i++)
            {
                for (int j = 0; j < nCol; j++)
                {
                    txtCell = dgv.Rows[i].Cells[j];
                    string s = txtCell.Value.ToString();
                    if (s == "")
                        matrix[i][j] = 0;
                    else matrix[i][j] = Int32.Parse(s);
                }
            }
        }


        public void MatrToGrid(DataGridView dgv)
        {
            DataTable matr = new DataTable("matr");
            DataColumn[] cols = new DataColumn[nCol];

            for (int i = 0; i < nCol; i++)
            {
                cols[i] = new DataColumn((i+1).ToString());
                matr.Columns.Add(cols[i]);
            }

            for (int i = 0; i < nStr; i++)
            {
                DataRow newRow;
                newRow = matr.NewRow();
                matr.Rows.Add(newRow);
            }
            dgv.DataSource = matr;

            for (int i = 0; i < nCol; i++)
                dgv.Columns[i].Width = 50;

            DataGridViewCell txtCell;
            for (int i = 0; i < nStr; i++)
                for (int j = 0; j < nCol; j++)
                {
                    txtCell = dgv.Rows[i].Cells[j];
                    txtCell.Value = matrix[i][j].ToString();
                }
        }


        public void Task()
        {
            int[][] resMtr = new int[2 * nStr][];
            int curRow = 0;
            for (int i = 0; i < nStr; i++)
            {
                resMtr[curRow] = NewRow(matrix[i]);
                if (IsOrd(matrix[i]))
                {
                    resMtr[curRow+1] = NewRow(matrix[i]);
                    ++curRow;
                    Array.Reverse(resMtr[curRow]);
                }
                ++curRow;
            }
            Array.Resize(ref resMtr, curRow);
            nStr = curRow;
            matrix = resMtr;
        }


        private bool IsOrd(int[] row)
        {
            bool isOrd = true;
            for (int i = 1; i<nCol && isOrd; i++)
            {
                isOrd = row[i - 1] < row[i];
            }
                
            return isOrd;
        }

        private int[] NewRow(int[] row)
        {
            int[] res = new int[nCol];
            for (int i = 0; i < nCol; i++)
            {
                res[i] = row[i];
            }
                
            return res;
        } 
    }
}
