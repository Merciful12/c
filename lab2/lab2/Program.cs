﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Введите значение X0");
            double x0 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите значение Xn");
            double xn = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Ввдите шаг");
            double h = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите значение эпсилон");
            double eps = Convert.ToDouble(Console.ReadLine());
            double curX = x0;
            Console.WriteLine();
            int j;
            while (curX<xn)

            {
                j = 0;
                curX = x0;
                Console.WriteLine();
                Console.Write("\n{0,-25}| ", "X");
                while (curX <= xn && j < 7)
                {                          
                    Console.Write("{0,8:f5};  ", curX);
                    curX += h;
                    j++;
                    
                }
                curX = x0;
                j = 0;
                Console.Write("\n{0,-25}| ", "f(x) вычисл. по формуле");
                while (curX <= xn && j < 7)
                {
                    double sum = 1.0 / (Math.Log(Math.E, (1 + curX) / (1 - curX)));
                    Console.Write("{0,8:f5};  ", sum);
                    curX += h;
                    j++;
                }
                curX = x0;
                j = 0;
                Console.Write("\n{0,-25}| ", "F(x) вычисл.");
                while (curX <= xn && j <7 )
                {
                    double sum = Sum(curX, eps);
                    Console.Write("{0,8:f5};  ", sum);
                    curX += h;
                    j++;
                }
                x0 = curX;
            }

            


            Console.ReadKey();
        }
       
        static double Sum(double x, double eps)
        {
            if (Math.Abs(2 * x) < eps)
                return 0;
            double res = 2 * x;
            double x2 = x * x;
            int k = 1;
            double cur = 2 * x;
            while (Math.Abs(cur) >= eps)
            {
                cur = (2 * x * x2) / (k + 2);
                res += cur;
                x *= x2;
                k += 2;
            }
            return res;
        }
    }   
}
