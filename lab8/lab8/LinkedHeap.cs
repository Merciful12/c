﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace lab8
{
    class LNode<T> where T : IComparable
    {
        public T Data { get; set; }
        public LNode<T> Left { get; set; }
        public LNode<T> Right { get; set; }
        public LNode<T> Parent { get; set; }

            
        public LNode(T inf, LNode<T> parent)
        {
            Data = inf;
            Left = null;
            Right = null;
            Parent = parent;
        }
    }

    class LinkedHeap<T>: IHeap<T> where T: IComparable
    {      
        public int Count { get; private set; }
        private LNode<T> Root { get; set; }
        private LNode<T> Last { get; set; }


        public LinkedHeap(T info)
        {
            Root = new LNode<T>(info, null);
            Count = 1;
        }

        public LinkedHeap()
        {
            Root = null;
            Count = 0;
        }

        public LinkedHeap(T[] data)
        {
            foreach (var t in data)
            {
                Add(t);
            }
        }

        public bool IsEmpty => Count == 0;

        public T[] Nodes
        {
            get
            {
                T[] array = new T[Count];
                int i = 0;
                var qu = new Queue<LNode<T>>();
                qu.Enqueue(Root);
                while (qu.Count > 0)
                {
                    var cur = qu.Dequeue();
                    if (cur != null)
                    {
                        array[i++] = cur.Data;
                        qu.Enqueue(cur.Left);
                        qu.Enqueue(cur.Right);
                    }
                }
                
                return array;
            }
        }


        private LNode<T> GetPosAdd()
        {
            if (Root == null) return null;
           
            var qu = new Queue<LNode<T>>();
            qu.Enqueue(Root);
            LNode<T> cur = null;
            while (qu.Count > 0)
            {
                cur = qu.Dequeue();

                if (cur.Left == null || cur.Right == null)
                    qu.Clear();
                else
                {
                    if (cur.Left != null) qu.Enqueue(cur.Left);
                    if (cur.Right != null) qu.Enqueue(cur.Right);
                }
            }
                
            return cur;
        }

        public void Add(T node)
        {
            if (node == null) throw new HeapException("value is null");
            if (Contains(node)) throw new HeapException("value already in Heap");

            LNode<T> res = GetPosAdd();
            var info = new LNode<T>(node, res);

            if (res == null)
                Root = info;
            else
            {
                if (res.Left == null)
                    res.Left = info;
                else
                    res.Right = info;
            }

            Last = info;                        
            Count++;
            CascadeUp(info);
        
        }

        private void CascadeUp(LNode<T> node)
        {
            var parent = node.Parent;
            var cur = node;

            while (cur.Parent != null && parent.Data.CompareTo(cur.Data) < 0)
            {
                Swap(cur, parent);
                cur = parent;
                parent = parent.Parent;
            }
        }
        private void Swap(LNode<T> a, LNode<T> b)
        {
            var temp = a.Data;
            a.Data = b.Data;
            b.Data = temp;
        }

        public void Remove(T node)
        {
            if (node == null) throw new HeapException("value is null");
            if (!Contains(node)) throw new HeapException("Heap has't value");

            var qu = new Queue<LNode<T>>();
            qu.Enqueue(Root);
            LNode<T> inf = Root;
            while (qu.Count > 0)
            {
                var cur = qu.Dequeue();
                if (cur != null)
                {
                    if (node.CompareTo(cur.Data) == 0)
                    {
                        qu.Clear();
                        inf = cur;
                    }

                    if (node.CompareTo(cur.Data) < 0)
                    {
                        qu.Enqueue(cur.Left);
                        qu.Enqueue(cur.Right);
                    }
                }
            }

            Swap(inf, Last);
            if (Last.Parent.Left == Last)
            {
                Last = Last.Parent;
                Last.Left = null;
            }
            else
            {
                Last = Last.Parent.Left;
                Last.Parent.Right = null;
            }
            CascadeDown(inf);
            Count--;
        }

        private void CascadeDown(LNode<T> node)
        {
            var largest = node;
            do
            {
                if (node.Left != null && largest.Data.CompareTo(node.Left.Data) < 0)
                    largest = node.Left;

                if (node.Right != null && largest.Data.CompareTo(node.Right.Data) < 0)
                    largest = node.Right;

                Swap(node, largest);
                node = largest;

            } while (largest != node);
            
        }

        private void LocalPrint(TreeNode treeNode, LNode<T> node)
        {
            treeNode.Text = Convert.ToString(node.Data);
            if (node.Left == null && node.Right == null) return;

            TreeNode rightNode = new TreeNode();
            TreeNode leftNode = new TreeNode();
            treeNode.Nodes.Add(rightNode);
            treeNode.Nodes.Add(leftNode);

            if (node.Left == null)
                leftNode.Text = @"*";
            else
                LocalPrint(leftNode, node.Left);


            if (node.Right == null)
                rightNode.Text = @"*";
            else
                LocalPrint(rightNode, node.Right);
        }

        public void Print(TreeView treeView)
        {
            treeView.Nodes.Clear();
            TreeNode rootNode = new TreeNode();
            treeView.Nodes.Add(rootNode);
            LocalPrint(rootNode, Root);
        }
        

        public void Clear()
        {
            Root = null;
            Count = 0;
        }

        public bool Contains(T node)
        {
            if (Count == 0) return false;
            var qu = new Queue<LNode<T>>();
            qu.Enqueue(Root);

            while (qu.Count > 0)
            {
                var cur = qu.Dequeue();
                if (cur != null)
                {
                    if (node.CompareTo(cur.Data) == 0)
                    {
                        qu.Clear();
                        return true;
                    }

                    if (node.CompareTo(cur.Data) < 0)
                    {
                        qu.Enqueue(cur.Left);
                        qu.Enqueue(cur.Right);
                    }
                }
                
            }
            return false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            if (Root == null) yield break;

            foreach (var node in Nodes)
            {
                yield return node;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
