﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace lab8
{
    interface IHeap<T>: IEnumerable<T> where T : IComparable
    {
        void Add(T node);
        void Clear();
        bool Contains(T node);
        void Remove(T node);
        void Print(TreeView tv);
            
        int Count { get; }
        bool IsEmpty { get; }
        T[] Nodes { get; }
    }
}
