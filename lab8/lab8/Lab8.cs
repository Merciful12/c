﻿using System;
using System.Windows.Forms;

namespace lab8
{
    public partial class Lab8 : Form
    {
        private HeapGUI _heap;
        private ValueType _vt;
        private HeapType _ht;    

        public Lab8()
        {
            InitializeComponent();
        }

        public void Application_Idle(object sender, EventArgs e)
        {
            treeView1.Visible = tbValue.Visible =
            btnAdd.Visible = btnClear.Visible = btnRemove.Visible =
            label1.Visible = btnFind.Visible = btnMute.Visible = _heap != null;
            functionsToolStripMenuItem.Enabled = _heap != null && !_heap.IsEmpty(_vt);
            btnAdd.Enabled = btnFind.Enabled = btnRemove.Enabled = tbValue.Text != "";

            _ht = rdArray.Checked ? HeapType.IsArray : HeapType.IsList;
            _vt = rdInt.Checked ? ValueType.IsIntValue : ValueType.IsStringValue;
        }
      

        private void RdHeapType_CheckChanged(object sender, EventArgs e)
        {            
            if (_heap == null) return;
            var res = MessageBox.Show(@"heap will be cleared are you sure?", @"Change Type of Heap", MessageBoxButtons.YesNo);
            if (res == DialogResult.Yes)
                _heap = null;
            else
            {
                RadioButton rb = (RadioButton)sender;
                rdArray.Checked = rb != rdArray;
                rdLinked.Checked = rb != rdLinked;
            }
        }   

        private void RdValueType_CheckChanged(object sender, EventArgs e)
        {
            if (_heap == null) return;
            var res = MessageBox.Show(@"Heap will be cleared are you sure?", @"Change Type of Heap", MessageBoxButtons.YesNo);
            if (res == DialogResult.Yes)
                _heap = null;
            else
            {
                RadioButton rb = (RadioButton)sender;
                rdInt.Checked = rb != rdInt;
                rdString.Checked = rb != rdString;
            }            
        }   

        private void btnCreate_Click(object sender, EventArgs e)
        {
            _heap = new HeapGUI(_vt, _ht);
            treeView1.Nodes.Clear();
        }

        private void btnMute_Click(object sender, EventArgs e)
        {
            btnMute.Text = btnMute.Text == @"Mute" ? "Unmute" : "Mute";
            _heap.ChangeMutability(btnMute.Text == @"Mute");
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {   
            if (!_heap.Remove(tbValue.Text)) return;
            _heap.Print(treeView1);
            tbValue.Clear();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (!_heap.Add(tbValue.Text)) return;
            _heap.Print(treeView1);
            tbValue.Clear();
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            _heap.Find(tbValue.Text);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (!_heap.Clear()) return;
            tbValue.Clear();
            treeView1.Nodes.Clear();
        }

        private void forEach2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _heap.ForEach();
            _heap.Print(treeView1);
        }

        private void findAllчетныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _heap.FindAll();
        }

        private void existToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(_heap.Exists() ? "Heap has some even numbers" : "Heap has't some even numbers");
        }

        private void checkForAllнечетныеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(_heap.CheckForAll() ? "All numbers are even" : "Not all numbers are even");
        }
 
    }
}
