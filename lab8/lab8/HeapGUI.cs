﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace lab8
{
    public enum ValueType { IsIntValue, IsStringValue };
    public enum HeapType { IsArray, IsList };
        
    class HeapGUI
    {
        private IHeap<int> _intHeap;
        private IHeap<string> _stringHeap;
        public ValueType VType;  
        public HeapType HType;
        private bool _isChangeable = true; 
    
        public HeapGUI()    
        {
            VType = ValueType.IsIntValue;
            HType = HeapType.IsArray;
            _isChangeable = true;
            _intHeap = new ArrayHeap<int>();
            _stringHeap = new ArrayHeap<string>();
        }

        public HeapGUI(ValueType vt, HeapType st)
        {
            VType = vt;
            HType = st;
            if (HType == HeapType.IsArray)
            {
                if (VType == ValueType.IsIntValue)
                {
                    _intHeap = new ArrayHeap<int>();
                }
                else
                {
                    _stringHeap = new ArrayHeap<string>();
                }

            }
            else
            {
                if (VType == ValueType.IsIntValue)
                {
                    _intHeap = new LinkedHeap<int>();
                }
                else
                {
                    _stringHeap = new LinkedHeap<string>();
                }
            }
        }

        public bool IsEmpty(ValueType ht)
        {
            return ht == ValueType.IsIntValue ? _intHeap.IsEmpty : _stringHeap.IsEmpty;
        }

        public void Print(TreeView treeView)
        {
            try
            {
                if (VType == ValueType.IsIntValue)
                {
                    _intHeap.Print(treeView);
                }
                else
                {
                    _stringHeap.Print(treeView);
                }
                treeView.ExpandAll();
            }
            catch (HeapException)
            {
                treeView.Nodes.Clear();
            }
        }

        public bool Add(string inputValue)
        {
            try
            {
                if (VType == ValueType.IsIntValue)
                {
                    _intHeap.Add(int.Parse(inputValue));
                }
                else
                {
                    _stringHeap.Add(inputValue);
                }

                return true;
            }

            catch (FormatException e)
            {
                MessageBox.Show(e.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (HeapException e)
            {
                MessageBox.Show(e.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Clear()
        {
            try
            {
                if (VType == ValueType.IsIntValue)
                {
                    _intHeap.Clear();
                }
                else
                {
                    _stringHeap.Clear();
                }
                return true;
            }
            
            catch (HeapException e)
            {
                MessageBox.Show(e.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        
        public bool Remove(string inputValue)
        {
            try
            {
                
                if (VType == ValueType.IsIntValue)
                {
                    _intHeap.Remove(int.Parse(inputValue));
                }
                else
                {
                    _stringHeap.Remove(inputValue);
                }

                return true;
            }
            
            catch (FormatException e)
            {
                MessageBox.Show(e.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (HeapException e)
            {
                MessageBox.Show(e.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }
        
        public void Find(string inputValue)
        {
            try
            {
                bool res = VType == ValueType.IsIntValue ? _intHeap.Contains(int.Parse(inputValue)) : _stringHeap.Contains(inputValue);

                MessageBox.Show(res ? "Heap has the value" : "Heap has't the value",
                                 @"Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (HeapException e)
            {
                MessageBox.Show(e.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
            
        public void ChangeMutability(bool isChangeable)
        {
            _isChangeable = isChangeable;
            if (!_isChangeable) 
            {
                if (VType == ValueType.IsIntValue)
                {
                    _intHeap = new UnmutableHeap<int>(_intHeap);
                }
                else
                {
                    _stringHeap = new UnmutableHeap<string>(_stringHeap);
                }
            }
            else
            {
                if (VType == ValueType.IsIntValue)
                {
                    if (HType == HeapType.IsArray)
                    {
                        _intHeap = new ArrayHeap<int>(_intHeap.Nodes);
                    }
                    else
                    {
                        _intHeap = new LinkedHeap<int>(_intHeap.Nodes);
                    }
                }
                else
                {
                    if (HType == HeapType.IsArray)
                    {
                        _stringHeap = new ArrayHeap<string>(_stringHeap.Nodes);
                    }
                    else
                    {
                        _stringHeap = new LinkedHeap<string>(_stringHeap.Nodes);
                    }
                }
            }
        }   
   
        private void ActionWithInt(int el, out int elem)
        {   
            elem = el * 2;
        }
        private void ActionWithString(string el, out string elem)
        {
            elem = Convert.ToString(Int32.Parse(el) * 2);
        }

        public bool ForEach()
        {
            try
            {
                if (VType == ValueType.IsIntValue)
                {
                    IHeap<int> intHeap0 = new ArrayHeap<int>();
                    HeapUtils.ForEach(_intHeap, ref intHeap0, ActionWithInt);
                    _intHeap = intHeap0;
                }
                else
                {
                    IHeap<string> stringHeap0 = new ArrayHeap<string>();
                    HeapUtils.ForEach(_stringHeap, ref stringHeap0, ActionWithString);
                    _stringHeap = stringHeap0;
                }
                return true;

            }
            catch (FormatException e)
            {
                MessageBox.Show(e.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (HeapException e)
            {
                MessageBox.Show(e.Message, @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private bool CheckInt(int elem)
        {
            return elem % 2 == 0;
        }

        private bool CheckString(string elem)
        {
            return int.Parse(elem) % 2 == 0;
        }

        public void FindAll()
        {
            string tmp = "";
            if (VType == ValueType.IsIntValue)
            {
                IHeap<int> resHeap = HeapUtils.FindAll(_intHeap, CheckInt, HeapUtils.ArrayHeapConstructor<int>());
                tmp = resHeap.Count > 0 ? 
                    resHeap.Nodes.Aggregate(tmp, (current, x) => current + $"( {x.ToString()} ) ")
                    : "Elements not find";
            }
            else
            {
                IHeap<string> resHeap = HeapUtils.FindAll(_stringHeap, CheckString, HeapUtils.ArrayHeapConstructor<string>());
                tmp = resHeap.Count > 0 ? 
                    resHeap.Nodes.Aggregate(tmp, (current, x) => current + $"( {x} ) ") 
                    : "Elements not find";
            }

            MessageBox.Show(tmp);
        }
        public bool CheckForAll()
        {
            if (VType == ValueType.IsIntValue)
            {
                return HeapUtils.CheckForAll(_intHeap, CheckInt);
            }
            else
            {
                return HeapUtils.CheckForAll(_stringHeap, CheckString);
            }
        }
        public bool Exists()
        {
            return VType == ValueType.IsIntValue ? HeapUtils.Exists(_intHeap, CheckInt) : HeapUtils.Exists(_stringHeap, CheckString);
        }
    }
}
