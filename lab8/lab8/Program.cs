﻿using System;
using System.Windows.Forms;

namespace lab8
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var form = new Lab8();
            Application.Idle += form.Application_Idle;
            Application.Run(form);
        }
    }
}
