﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace lab8
{
    class Node<T>
    {
        public T Data { get; set; }
        public Node(T data) => Data = data;
    }

    class ArrayHeap<T>: IHeap<T> where T : IComparable
    {
        public int Count { get; private set; }
        private const int N = 100;
        private Node<T>[] _heapArray = new Node<T>[N];

        public ArrayHeap(T info)
        {
            Add(info);        
        }

        public ArrayHeap() => Count = 0;

        public ArrayHeap(T[] data)
        {
            foreach (var t in data)
            {
                Add(t);
            }
        }

        public bool IsEmpty => Count == 0;

        public T[] Nodes
        {
            get
            {
                T[] array = new T[Count];
                int i = 0;
                foreach (var node in _heapArray)
                {
                    if (node != null)
                    {
                        array[i++] = node.Data;
                    }
                }
                return array;
            }
        }


        public IEnumerator<T> GetEnumerator()
        {            
            int i = 1;

            if (_heapArray[i] == null) yield break;

            var qu = new Queue<int>();
            qu.Enqueue(i);
            int cnt = 1;
            while (cnt <= Count)
            {
                i = qu.Dequeue();
                if (_heapArray[i] != null)
                {
                    yield return _heapArray[i].Data;
                    qu.Enqueue(i * 2);
                    qu.Enqueue(i * 2 + 1);
                    cnt++;
                }                    
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T node)
        {
            if (node == null) throw new HeapException("value is null");
            if (Contains(node)) throw new HeapException("value already in Heap");
            if (Count + 1 == N) throw new HeapException("Heap is full");

            var info = new Node<T>(node);
            _heapArray[++Count] = info;
    
            CascadeUp(Count);
        }

        private void CascadeUp(int index)
        {
            int parent = index / 2;
            int cur = index;
            while (cur > 1 && _heapArray[parent].Data.CompareTo(_heapArray[cur].Data) < 0)
            {
                Swap(cur, parent);
                cur = parent;
                parent = parent / 2;
            }          
        }

        private void Swap(int a, int b)
        {
            var temp = _heapArray[a];
            _heapArray[a] = _heapArray[b];
            _heapArray[b] = temp;
        }

        public void Remove(T node)
        {
            if (node == null) throw new HeapException("value is null");
            if (!Contains(node) || Count == 0) throw new HeapException("Heap has't value");

            int i;
            for ( i = 1; i <= Count; i++)
            {
                if (node.CompareTo(_heapArray[i].Data) == 0)
                    break;
            }

            _heapArray[i] = _heapArray[Count];
            _heapArray[Count] = null;
            CascadeDown(i);
            Count--;
        }

        private void CascadeDown(int index)
        {
            int largest = index;
            do
            {
                int left = 2 * index,
                    right = 2 * index + 1;

                if (left < Count && _heapArray[largest].Data.CompareTo(_heapArray[left].Data) < 0)
                    largest = left;

                if (right < Count && _heapArray[largest].Data.CompareTo(_heapArray[right].Data) < 0)
                    largest = right;

                Swap(index, largest);
                index = largest;
            } while (largest != index);

        }

        public void Clear()
        {
            _heapArray = new Node<T>[N];
            Count = 0;
        }

        public bool Contains(T node)
        {
            for (int i = 1; i <= Count; i++)
            {
                if (node.CompareTo(_heapArray[i].Data) == 0)
                    return true;
            }
            return false;
        }   

        private void LocalPrint(TreeNode treeNode, int i)
        {
            treeNode.Text = Convert.ToString(_heapArray[i].Data);
            if ((i * 2 > Count) && (i * 2 + 1 > Count)) return;

            TreeNode rightNode = new TreeNode();
            TreeNode leftNode = new TreeNode();
            treeNode.Nodes.Add(rightNode);
            treeNode.Nodes.Add(leftNode);
            if (i * 2 > Count)
                leftNode.Text = @"*";
            else
            {
                LocalPrint(leftNode, i * 2);

            }

            if (i * 2 + 1 > Count)
                rightNode.Text = @"*";
            else
            {
                LocalPrint(rightNode, i * 2 + 1);
            }
        }

        public void Print(TreeView treeView)
        {
            treeView.Nodes.Clear();
            TreeNode rootNode = new TreeNode();
            treeView.Nodes.Add(rootNode);
            LocalPrint(rootNode, 1);
        }
    }
}
