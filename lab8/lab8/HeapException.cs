﻿using System;

namespace lab8
{
    class HeapException: Exception
    {
        public HeapException() { }

        public HeapException(string message): base(message) { }

        public HeapException(string message, Exception inner): base(message, inner) { }
    }
}
