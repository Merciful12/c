﻿namespace lab8
{
    partial class Lab8
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnFind = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.tbValue = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCreate = new System.Windows.Forms.Button();
            this.realizationTree = new System.Windows.Forms.GroupBox();
            this.rdArray = new System.Windows.Forms.RadioButton();
            this.rdLinked = new System.Windows.Forms.RadioButton();
            this.typeOfTree = new System.Windows.Forms.GroupBox();
            this.rdString = new System.Windows.Forms.RadioButton();
            this.rdInt = new System.Windows.Forms.RadioButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.functionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsForEach = new System.Windows.Forms.ToolStripMenuItem();
            this.tsFindAll = new System.Windows.Forms.ToolStripMenuItem();
            this.tsExist = new System.Windows.Forms.ToolStripMenuItem();
            this.tsCheckForAll = new System.Windows.Forms.ToolStripMenuItem();
            this.btnMute = new System.Windows.Forms.Button();
            this.realizationTree.SuspendLayout();
            this.typeOfTree.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Right;
            this.treeView1.Location = new System.Drawing.Point(191, 0);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(609, 450);
            this.treeView1.TabIndex = 1;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(8, 415);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(79, 23);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(8, 337);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(79, 23);
            this.btnFind.TabIndex = 9;
            this.btnFind.Text = "Find";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(8, 298);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(79, 23);
            this.btnAdd.TabIndex = 10;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(93, 298);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(79, 23);
            this.btnRemove.TabIndex = 11;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // tbValue
            // 
            this.tbValue.Location = new System.Drawing.Point(12, 272);
            this.tbValue.Name = "tbValue";
            this.tbValue.Size = new System.Drawing.Size(160, 20);
            this.tbValue.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 256);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Enter:";
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(8, 221);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 14;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // realizationTree
            // 
            this.realizationTree.Controls.Add(this.rdArray);
            this.realizationTree.Controls.Add(this.rdLinked);
            this.realizationTree.Location = new System.Drawing.Point(12, 130);
            this.realizationTree.Name = "realizationTree";
            this.realizationTree.Size = new System.Drawing.Size(127, 71);
            this.realizationTree.TabIndex = 15;
            this.realizationTree.TabStop = false;
            this.realizationTree.Text = "Base of heap";
            // 
            // rdArray
            // 
            this.rdArray.AutoSize = true;
            this.rdArray.Checked = true;
            this.rdArray.Location = new System.Drawing.Point(6, 19);
            this.rdArray.Name = "rdArray";
            this.rdArray.Size = new System.Drawing.Size(49, 17);
            this.rdArray.TabIndex = 1;
            this.rdArray.TabStop = true;
            this.rdArray.Text = "Array";
            this.rdArray.UseVisualStyleBackColor = true;
            this.rdArray.Click += new System.EventHandler(this.RdHeapType_CheckChanged);
            // 
            // rdLinked
            // 
            this.rdLinked.AutoSize = true;
            this.rdLinked.Location = new System.Drawing.Point(6, 42);
            this.rdLinked.Name = "rdLinked";
            this.rdLinked.Size = new System.Drawing.Size(57, 17);
            this.rdLinked.TabIndex = 0;
            this.rdLinked.Text = "Linked";
            this.rdLinked.UseVisualStyleBackColor = true;
            this.rdLinked.Click += new System.EventHandler(this.RdHeapType_CheckChanged);
            // 
            // typeOfTree
            // 
            this.typeOfTree.Controls.Add(this.rdString);
            this.typeOfTree.Controls.Add(this.rdInt);
            this.typeOfTree.Location = new System.Drawing.Point(12, 47);
            this.typeOfTree.Name = "typeOfTree";
            this.typeOfTree.Size = new System.Drawing.Size(127, 69);
            this.typeOfTree.TabIndex = 16;
            this.typeOfTree.TabStop = false;
            this.typeOfTree.Text = "Type of Heap";
            // 
            // rdString
            // 
            this.rdString.AutoSize = true;
            this.rdString.Location = new System.Drawing.Point(7, 42);
            this.rdString.Name = "rdString";
            this.rdString.Size = new System.Drawing.Size(52, 17);
            this.rdString.TabIndex = 1;
            this.rdString.Text = "String";
            this.rdString.UseVisualStyleBackColor = true;
            this.rdString.Click += new System.EventHandler(this.RdValueType_CheckChanged);
            // 
            // rdInt
            // 
            this.rdInt.AutoSize = true;
            this.rdInt.Checked = true;
            this.rdInt.Location = new System.Drawing.Point(7, 19);
            this.rdInt.Name = "rdInt";
            this.rdInt.Size = new System.Drawing.Size(36, 17);
            this.rdInt.TabIndex = 0;
            this.rdInt.TabStop = true;
            this.rdInt.Text = "int";
            this.rdInt.UseVisualStyleBackColor = true;
            this.rdInt.Click += new System.EventHandler(this.RdValueType_CheckChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.functionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(191, 24);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // functionsToolStripMenuItem
            // 
            this.functionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsForEach,
            this.tsFindAll,
            this.tsExist,
            this.tsCheckForAll});
            this.functionsToolStripMenuItem.Name = "functionsToolStripMenuItem";
            this.functionsToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.functionsToolStripMenuItem.Text = "Functions";
            // 
            // tsForEach
            // 
            this.tsForEach.Name = "tsForEach";
            this.tsForEach.Size = new System.Drawing.Size(174, 22);
            this.tsForEach.Text = "ForEach (*2)";
            this.tsForEach.Click += new System.EventHandler(this.forEach2ToolStripMenuItem_Click);
            // 
            // tsFindAll
            // 
            this.tsFindAll.Name = "tsFindAll";
            this.tsFindAll.Size = new System.Drawing.Size(174, 22);
            this.tsFindAll.Text = "FindAll (even)";
            this.tsFindAll.Click += new System.EventHandler(this.findAllчетныеToolStripMenuItem_Click);
            // 
            // tsExist
            // 
            this.tsExist.Name = "tsExist";
            this.tsExist.Size = new System.Drawing.Size(174, 22);
            this.tsExist.Text = "Exist (even)";
            this.tsExist.Click += new System.EventHandler(this.existToolStripMenuItem_Click);
            // 
            // tsCheckForAll
            // 
            this.tsCheckForAll.Name = "tsCheckForAll";
            this.tsCheckForAll.Size = new System.Drawing.Size(174, 22);
            this.tsCheckForAll.Text = "CheckForAll (even)";
            this.tsCheckForAll.Click += new System.EventHandler(this.checkForAllнечетныеToolStripMenuItem_Click);
            // 
            // btnMute
            // 
            this.btnMute.Location = new System.Drawing.Point(93, 415);
            this.btnMute.Name = "btnMute";
            this.btnMute.Size = new System.Drawing.Size(79, 23);
            this.btnMute.TabIndex = 18;
            this.btnMute.Text = "Mute";
            this.btnMute.UseVisualStyleBackColor = true;
            this.btnMute.Click += new System.EventHandler(this.btnMute_Click);
            // 
            // Lab8
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnMute);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.typeOfTree);
            this.Controls.Add(this.realizationTree);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbValue);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnFind);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.treeView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Lab8";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Heap";
            this.realizationTree.ResumeLayout(false);
            this.realizationTree.PerformLayout();
            this.typeOfTree.ResumeLayout(false);
            this.typeOfTree.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.TextBox tbValue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.GroupBox realizationTree;
        private System.Windows.Forms.RadioButton rdArray;
        private System.Windows.Forms.RadioButton rdLinked;
        private System.Windows.Forms.GroupBox typeOfTree;
        private System.Windows.Forms.RadioButton rdString;
        private System.Windows.Forms.RadioButton rdInt;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem functionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsForEach;
        private System.Windows.Forms.ToolStripMenuItem tsFindAll;
        private System.Windows.Forms.ToolStripMenuItem tsExist;
        private System.Windows.Forms.ToolStripMenuItem tsCheckForAll;
        private System.Windows.Forms.Button btnMute;
    }
}

