﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace lab8
{
    class UnmutableHeap<T> : IHeap<T> where T : IComparable
    {
        public IHeap<T> Heap;

        public UnmutableHeap(IHeap<T> heap) => Heap = heap;

        public int Count => Heap.Count;

        public bool IsEmpty => Heap.IsEmpty;

        T[] IHeap<T>.Nodes => Heap.Nodes;

        public void Add(T node) => throw new HeapException("Heap is unmutable");

        public void Clear() => throw new HeapException("Heap is unmutable");

        public bool Contains(T node) => Heap.Contains(node);

        public IEnumerator<T> GetEnumerator() => Heap.GetEnumerator();

        public void Print(TreeView tv) => Heap.Print(tv);

        public void Remove(T node) => throw new HeapException("Heap is unmutable");

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
