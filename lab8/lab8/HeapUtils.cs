﻿using System;


namespace lab8
{
    delegate bool CheckDelegate<T>(T source) where T : IComparable;

    delegate void ActionDelegate<T>(T source0, out T source) where T : IComparable;

    delegate IHeap<T> HeapConstructorDelegate<T>() where T : IComparable;

    class HeapUtils
    {
        public static bool Exists<T>(IHeap<T> heap, CheckDelegate<T> Delegate) where T : IComparable
        {
            foreach (T x in heap)
            {
                if (Delegate(x)) return true;
            }
            return false;
        }

        public static IHeap<T> FindAll<T>(IHeap<T> heap, CheckDelegate<T> chDelegate, HeapConstructorDelegate<T> coDelegate) where T : IComparable
        {
            IHeap<T> tmp = coDelegate();
            foreach (T x in heap)
            {
                if (chDelegate(x)) tmp.Add(x);
            }
            return tmp;
        }

        public static void ForEach<T>(IHeap<T> heap, ref IHeap<T> heap0, ActionDelegate<T> algDelegate) where T : IComparable
        {
            if (heap is ArrayHeap<T>)
            {
                heap0 = new ArrayHeap<T>();
                foreach (T x in heap)
                {
                    
                    algDelegate(x, out T y);
                    heap0.Add(y);
                    heap = heap0;
                    heap0 = heap;
                }
            }
            if (heap is LinkedHeap<T>)
            {
                heap0 = new LinkedHeap<T>();
                foreach (T x in heap)
                {
                    algDelegate(x, out T y);
                    heap0.Add(y);
                    heap = heap0;
                    heap0 = heap;
                }
            }
        }
        public static bool CheckForAll<T>(IHeap<T> heap, CheckDelegate<T> checkDelegate) where T : IComparable
        {
            if (heap.IsEmpty) return false;

            bool Checked = true;
            foreach (T x in heap)
            {
                Checked = Checked && checkDelegate(x);
            }
            return Checked;

        }

        public static HeapConstructorDelegate<T> ArrayHeapConstructor<T>() where T : IComparable
        {
            return Activator.CreateInstance<ArrayHeap<T>>;
        }
        public static HeapConstructorDelegate<T> LinkedHeapConstructor<T>() where T : IComparable
        {
            return Activator.CreateInstance<LinkedHeap<T>>;
        }
    }   
}
